package com.shekhar.doubtnutassignment.viewmodel.view.fragment

import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shekhar.doubtnutassignment.R
import com.shekhar.doubtnutassignment.viewmodel.base.BaseApp
import com.shekhar.doubtnutassignment.viewmodel.base.BaseFragment
import kotlinx.android.synthetic.main.users_fragment.*
import com.shekhar.doubtnutassignment.viewmodel.repository.data.News
import com.shekhar.doubtnutassignment.viewmodel.repository.data.NewsList
import com.shekhar.doubtnutassignment.viewmodel.view.adapter.NewsListAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NewsListFragment : BaseFragment() {

    val newsListViewModel = BaseApp.injectNewsListViewModel()
    private lateinit var newsListAdapter: NewsListAdapter
    private var list=ArrayList<News>()

    override fun layoutRes(): Int {
        return R.layout.users_fragment
    }

    override fun onStart() {
        super.onStart()
        newsListAdapter = NewsListAdapter(context , list)
        newsList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = this@NewsListFragment.newsListAdapter
        }
        subscribe(newsListViewModel.getNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showNews(it)
                }, {
                    showError()
                }))


        /*newsListViewModel.getLoading().observe(this, Observer {
            loading_view!!.visibility = if (it) View.VISIBLE else View.GONE
            if (it) {
                newsList!!.setVisibility(View.GONE)
            }
        })*/
    }

    fun showNews(data: NewsList) {

        if (data.status.equals("ok")) {
            newsListAdapter.setUserList(data.articles as ArrayList<News>)
        } else if (data.status.equals("error")) {
            showError()
        } else {
            showError()
        }
    }

    fun showError() {
        Toast.makeText(context, "An error occurred :(", Toast.LENGTH_SHORT).show()
    }
}
