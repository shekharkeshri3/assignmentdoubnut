package com.shekhar.doubtnutassignment.viewmodel.repository.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "News")
data class News(
                @PrimaryKey
                @ColumnInfo(name = "title")
                val title: String,
                @ColumnInfo(name = "description")
                val description: String,
                @ColumnInfo(name = "urlToImage")
                val urlToImage: String)
