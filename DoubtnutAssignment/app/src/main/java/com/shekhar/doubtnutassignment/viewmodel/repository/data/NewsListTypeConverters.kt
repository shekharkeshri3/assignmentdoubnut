package com.shekhar.doubtnutassignment.viewmodel.repository.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*


class NewsListTypeConverters{
    var gson = Gson()

    @TypeConverter
    fun fromTimestamp(data: String?): List<News>? {

        if (data == null){
            return Collections.emptyList()
        }
        val listType = object : TypeToken<List<News>>() {}.type
        return gson.fromJson(data, listType)


    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<News>?): String? {
        return gson.toJson(someObjects)
    }

}