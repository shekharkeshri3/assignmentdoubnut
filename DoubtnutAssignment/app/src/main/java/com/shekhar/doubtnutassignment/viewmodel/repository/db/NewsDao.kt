package com.shekhar.doubtnutassignment.viewmodel.repository.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.shekhar.doubtnutassignment.viewmodel.repository.data.NewsList
import io.reactivex.Maybe
import io.reactivex.Observable

@Dao
interface NewsDao {

    @Query("SELECT * FROM NewsList")
    fun getNews(): Maybe<NewsList>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(news: NewsList)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(news: NewsList)
}