package com.shekhar.doubtnutassignment.viewmodel.view.main

import android.os.Bundle
import com.shekhar.doubtnutassignment.R
import com.shekhar.doubtnutassignment.viewmodel.base.BaseActivity
import com.shekhar.doubtnutassignment.viewmodel.view.fragment.NewsListFragment

class MainActivity : BaseActivity() {

    override  fun layoutRes(): Int {
        return R.layout.activity_main
    }

    override  fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null)
            getSupportFragmentManager().beginTransaction().add(R.id.screenContainer, NewsListFragment()).commit()
    }
}
