package com.shekhar.doubtnutassignment.viewmodel.repository.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters


@Entity(tableName = "NewsList")
data class NewsList(
        @ColumnInfo(name = "status")
        val status: String,
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "totalResults")
        val totalResults: Int,
        @TypeConverters(NewsListTypeConverters::class)
        val articles: List<News>)


