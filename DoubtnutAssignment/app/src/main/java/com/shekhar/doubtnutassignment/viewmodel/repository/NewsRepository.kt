package com.shekhar.doubtnutassignment.viewmodel.repository

import android.annotation.SuppressLint
import android.util.Log
import com.shekhar.doubtnutassignment.viewmodel.repository.api.ApiManager
import com.shekhar.doubtnutassignment.viewmodel.repository.data.NewsList
import com.shekhar.doubtnutassignment.viewmodel.repository.db.NewsDao
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class NewsRepository(val userApi: ApiManager, val newsDao: NewsDao) {

    fun getNews(): Observable<NewsList> {
        return Observable.concatArray(
            getNewsFromDb(),
            getNewsFromApi()
        )
    }

    fun getNewsFromDb(): Observable<NewsList> {
        return newsDao.getNews().toObservable().doOnNext {
            Log.d("Dispatching", "Dispatching ${it.articles.size} users from DB...")
        }
    }

    fun getNewsFromApi(): Observable<NewsList> {
        return userApi.getNews()
            .doOnNext {
                Log.d("Dispatching", "Dispatching ${it.articles.size} users from API...")
                storeUsersInDb(it)
            }
    }

    @SuppressLint("CheckResult")
    fun storeUsersInDb(newsList: NewsList) {
        Observable.fromCallable { newsDao.insertAll(newsList) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe {
                Log.d("Inserted", "Inserted ${newsList.articles.size} users from API in DB...")
            }
    }

}
