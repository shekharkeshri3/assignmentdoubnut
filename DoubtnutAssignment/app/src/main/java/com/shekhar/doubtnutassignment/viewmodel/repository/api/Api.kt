package com.shekhar.doubtnutassignment.viewmodel.repository.api

import io.reactivex.Observable
import com.shekhar.doubtnutassignment.viewmodel.repository.data.NewsList
import retrofit2.http.GET

interface ApiManager {

    @GET("top-headlines?country=in&apiKey=65543c034f79472393e52480780cf275")
    fun getNews(): Observable<NewsList>
}
