package com.shekhar.doubtnutassignment.viewmodel.repository.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.shekhar.doubtnutassignment.viewmodel.repository.data.News
import com.shekhar.doubtnutassignment.viewmodel.repository.data.NewsList
import com.shekhar.doubtnutassignment.viewmodel.repository.data.NewsListTypeConverters

@Database(entities = [NewsList::class, News::class], version = 1)
@TypeConverters(NewsListTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
}