package com.shekhar.doubtnutassignment.viewmodel.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.shekhar.doubtnutassignment.R
import com.shekhar.doubtnutassignment.viewmodel.repository.data.News
import kotlinx.android.synthetic.main.news_list_item.view.*

class NewsListAdapter(
    private val mContext: Context?,
    private var list: ArrayList<News>) : RecyclerView.Adapter<NewsListAdapter.RecyclerViewHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.news_list_item, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindPerformersChild(list.get(position), position)
    }

    fun setUserList(list: ArrayList<News>){
        this.list=list
        notifyDataSetChanged()
    }
    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindPerformersChild(news: News, position: Int) {
            val requestOptions = RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)

            itemView.title_tv.text = news.title
            Glide.with(mContext!!)
                .setDefaultRequestOptions(requestOptions)
                .load(news.urlToImage)
                .into(itemView.image)

            itemView.setOnClickListener(View.OnClickListener {

            })
        }
    }
}