package com.shekhar.doubtnutassignment.viewmodel.base

import android.app.Application
import androidx.room.Room
import com.shekhar.doubtnutassignment.viewmodel.repository.NewsRepository
import com.shekhar.doubtnutassignment.viewmodel.repository.api.ApiManager
import com.shekhar.doubtnutassignment.viewmodel.repository.db.AppDatabase
import com.shekhar.doubtnutassignment.viewmodel.view.vm.NewsListViewModel
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class BaseApp : Application() {

    //For the sake of simplicity, for now we use this instead of Dagger
    companion object {
        private lateinit var retrofit: Retrofit
        private lateinit var apiManager: ApiManager
        private lateinit var newsRepository: NewsRepository
        private lateinit var newsListViewModel: NewsListViewModel
        private lateinit var appDatabase: AppDatabase
        private val BASE_URL="https://newsapi.org/v2/"

        fun injectUserApi() = apiManager
        fun injectNewsListViewModel() = newsListViewModel
        fun injectNewsDao() = appDatabase.newsDao()
    }

    override fun onCreate() {
        super.onCreate()
        retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build()

        apiManager = retrofit.create(ApiManager::class.java)
        appDatabase = Room.databaseBuilder(applicationContext,
            AppDatabase::class.java, "mvvm-database").build()

        newsRepository = NewsRepository(apiManager, appDatabase.newsDao())
        newsListViewModel = NewsListViewModel(newsRepository)
    }
}
