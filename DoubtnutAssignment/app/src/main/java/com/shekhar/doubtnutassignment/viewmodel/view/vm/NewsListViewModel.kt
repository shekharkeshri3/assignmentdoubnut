package com.shekhar.doubtnutassignment.viewmodel.view.vm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.shekhar.doubtnutassignment.viewmodel.repository.NewsRepository
import com.shekhar.doubtnutassignment.viewmodel.repository.data.NewsList
import io.reactivex.Observable

class NewsListViewModel(private val newsRepository: NewsRepository) {

    /*var loading: MutableLiveData<Boolean> = MutableLiveData()

    internal fun getLoading(): MutableLiveData<Boolean> {
        return loading
    }*/

    fun getNews(): Observable<NewsList> {
//        loading.value=true
        //Create the data for your UI, the users lists and maybe some additional data needed as well
        return newsRepository.getNews()
                .map {
//                    loading.value=false
                    Log.d("Mapping","Mapping users to UIData...")
                    NewsList( it.status,it.totalResults,it.articles)
                }
                .onErrorReturn {
//                    loading.value=false
                    Log.d("Mapping","An error occurred $it")
                    NewsList("An error occurred", 0,emptyList())
                }
    }

}
